package com.example.pry_pagos_volley.modelo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pry_pagos_volley.R

//este adaptador va recibir la lista de artefactos y lo asiganar a los controles de la vista
class adaptador1(private val mlis:List<Alu_Nota>, private val itemClick:onAlumOnclick):RecyclerView.Adapter<adaptador1.ViewHolder>() {

    interface onAlumOnclick{
        fun onBotonClick(a: Alu_Nota)
        fun onBotonClick2(a: Alu_Nota)
    }
    class ViewHolder(item:View):RecyclerView.ViewHolder(item) {
      //para programar eventos en la seleccion de un item

        //crear variables de programa para cada control de la vista

        val cod=item.findViewById<TextView>(R.id.txtCodigo)
        val nombre=item.findViewById<TextView>(R.id.txtNombre)
        val ape=item.findViewById<TextView>(R.id.txtApellido)
        val btnNotas=item.findViewById<Button>(R.id.btnNotas)
        val btnPagos=item.findViewById<Button>(R.id.btnPagos)
      }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vista=LayoutInflater.from(parent.context).inflate(R.layout.vista1,parent,false)
        return ViewHolder(vista)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      //relacionar las variables con el dato
        var a: Alu_Nota =mlis.get(position)  //capturar el item de la lista
        holder.cod.setText(a.coda)
        holder.nombre.setText(a.nom)
        holder.ape.setText(a.ape)
        holder.btnNotas.setOnClickListener{
            itemClick.onBotonClick(a)
        }
        holder.btnPagos.setOnClickListener{
            itemClick.onBotonClick2(a)
        }

    }

    override fun getItemCount(): Int {
        return mlis.size
    }

}
