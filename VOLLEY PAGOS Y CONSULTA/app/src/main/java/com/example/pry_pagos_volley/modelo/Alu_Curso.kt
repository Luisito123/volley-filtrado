package com.example.pry_pagos_volley.modelo

import java.io.Serializable

class Alu_Curso: Serializable {
    var codc:String=""
    var nomc:String=""
    var ep:Int=0
    var ef:Int=0
    fun prom():Double{
        return(ep+ef)/2.0
    }
}
